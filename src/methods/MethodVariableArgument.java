package methods;

public class MethodVariableArgument {

    public static void main(String[] args) {
        getAverage(1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1);
    }

    static void getAverage(int... nums){
        var total = 0;
        for (var num : nums) {
            total += num;
        }
        var average = total / nums.length;
        System.out.println("rata-rata adalah : "+average);
    }
}
