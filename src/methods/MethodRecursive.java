package methods;

import java.math.BigInteger;

public class MethodRecursive {
    public static void main(String[] args) {
        var factorial = funRecursive(10);
        String numberWithSeparator = String.format("%,d", factorial);
        System.out.println("result factorial = " + numberWithSeparator);

    }

    static BigInteger funRecursive(int num){
        if(num == 1){return BigInteger.valueOf( 1);}
        return BigInteger.valueOf(num).multiply(funRecursive(num - 1));
    }
}
