package methods;

public class MethodReturnValue {
    public static void main(String[] args) {
        var funReturnValue = funReturnValue(32);
        System.out.println("return value = " + funReturnValue);
    }

    static int funReturnValue(int param){
        return (param * 2);
    }
}
