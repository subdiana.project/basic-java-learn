package methods;

public class MethodOverloading {

    public static void main(String[] args) {
        methodOverloading();
        methodOverloading("data");
    }

    static void methodOverloading() {
        System.out.println("Hello World from main method");
    }
    static void methodOverloading( String param) {
        System.out.println("Hello World from overloading method with param : " + param);
    }
}
