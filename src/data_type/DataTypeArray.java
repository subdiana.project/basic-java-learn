package data_type;

import java.util.Arrays;

public class DataTypeArray {
    public static void main(String[] args) {
        String[] arrayString = new String[3];
        arrayString[0] = "a";
        arrayString[1] = "b";
        arrayString[2] = "c";

        System.out.print("arrays => ");
        System.out.println(Arrays.toString(arrayString));
        System.out.print("arrays ke 2 => ");
        System.out.println(arrayString[1]);

        int[] arrayInt = {
                1, 2, 3, 4, 5
        };

        System.out.println("array int => " + Arrays.toString(arrayInt));
        System.out.println("array length => " + arrayInt.length);

        String[][] member = {
                {"deo", "izy"},
                {"iyan", "neny"}
        };

        System.out.println("Array 2D => " + Arrays.deepToString(member));

    }
}
