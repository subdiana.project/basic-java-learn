package loop;

public class DoWhileLoop {
    public static void main(String[] args) {
        var counter = 1;
        do {
            System.out.println("counter = " + counter);
            counter++;
        } while (counter < 10);
    }
}
