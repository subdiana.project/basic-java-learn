package loop;

public class ForLoop {
    public static void main(String[] args) throws InterruptedException {
        for (int i = 1; i <= 10; i++) {
            System.out.println("for loop ke : " + i);
            Thread.sleep(500);
        }

        var nilai = 1;
        for (;;nilai++){
            System.out.println("infinite loop ke ; " + nilai);
            Thread.sleep(1000);
        }

    }
}
