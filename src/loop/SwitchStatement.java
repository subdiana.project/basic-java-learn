package loop;

public class SwitchStatement {
    public static void main(String[] args) {
        var nilai = "C";

        switch (nilai) {
            case "A":
                System.out.println("nilai = A");
                break;
            case "B":
            case "C":
                System.out.println("nilai = B atau C  ");
                break;
            case "D":
                System.out.println("Nilai = D");
                break;
            default:
                System.out.println("nilai tidak ketemu ");

        }

        System.out.println();
        System.out.println("Switch Lamda ...");

        var nilaiLambda = "X";
        switch (nilaiLambda){
            case "A" -> {
                System.out.println(" Switch lamda = A");
            }
            case "B", "C" -> {
                System.out.println(" Switch lamda = B/C");
            }
            case "D" -> {
                System.out.println(" Switch lamda = D");
            }
            default -> {
                System.out.println(" nilai lamda tidak ditemukan");
            }
        }

        System.out.println();
        System.out.println("Switch yield ...");

        var nilaiYield = "B";
        var returnYield = switch (nilaiYield){
            case "A" -> {
                yield "yield : a";
            }
            case "B", "C" -> {
                yield "yield : B/C";
            }
            default -> {
                yield "not defind";
            }
        };
        System.out.println(returnYield);


    }
}
