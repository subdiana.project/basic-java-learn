package loop;

public class BreakNContinue {
    public static void main(String[] args) {
        var counter = 1;
        while(true){
            System.out.println("break counter = " + counter);
            counter++;

            if (counter == 10){break;}
        }

        for (int i = 1; i <= 20 ; i++) {
            if (i % 2 == 0){
                continue;
            }
            System.out.println("continue counter = " + i);
        }
    }
}
