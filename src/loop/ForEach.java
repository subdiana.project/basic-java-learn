package loop;

public class ForEach {
    public static void main(String[] args) {
        String[] names = {"iyan", "subdiana", "hamizan", "khawarizmi"};

        for (var name : names) {
            System.out.println(name);
        }
    }
}
