package operation;

public class MathOperation {
    public static void main(String[] args) {
        int a = 100;
        int b = 10;

        System.out.println("a = 100 | b = 10 ");
        System.out.println("a + b = " + (a+b));
        System.out.println("a - b = " + (a-b));
        System.out.println("a * b = " + (a*b));
        System.out.println("a / b = " + (a/b));
        System.out.println("a % 3 = " + (a%3));


        /*Augmented Assigment*/

        System.out.println();
        int c = 100;
        System.out.println("Augmented Assigment [c = 100]");

        c += 10;
        System.out.println("c += 10 : " + c);

        c -= 30;
        System.out.println("c -= 30 : " + c);

        c *= 5;
        System.out.println("c *= 5 : " + c);

        c %= 33;
        System.out.println("c %= 33 : " + c);

        System.out.println();
        System.out.println("Unary Operator [d = 100, e = -10]");

        int  d = 100;
        int e = -10;

        d++;
        System.out.println("d++ = " + d);

        e--;
        System.out.println("e-- = " + e);

        System.out.println("!true = " + !true);
    }
}
