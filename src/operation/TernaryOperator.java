package operation;

public class TernaryOperator {
    public static void main(String[] args) {

        var nilai = 30;
        String strReturn = ( nilai >= 50) ? "lebih besar" : "Lebih kecil";
        System.out.println(strReturn);
    }
}
