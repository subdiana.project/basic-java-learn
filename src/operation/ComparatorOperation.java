package operation;

public class ComparatorOperation {
    public static void main(String[] args) {

        int a = 10;
        int b = 10;

        System.out.println("Comparator Operation ... ");
        System.out.println("a > b : " + (a > b));
        System.out.println("a < b : " + (a < b));
        System.out.println("a >= b : " + (a >= b));
        System.out.println("a <= b : " + (a <= b));
        System.out.println("a == b : " + (a == b));
        System.out.println("a != b : " + (a != b));

    }
}
