package operation;

import java.util.Date;

public class ExpressionStatement {
    public static void main(String[] args) {

        /*Assigment Statement*/
        double dValue = 78.778;

        /*Increment Statement*/
        dValue++;

        /*methods.Method invocation statement*/
        System.out.println("value double = " + (dValue));

        /*Object Creation Statement*/
        Date date = new Date();
        System.out.println("date = " + date);
    }
}
